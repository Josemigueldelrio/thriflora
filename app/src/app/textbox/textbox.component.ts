import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-textbox',
    templateUrl: './textbox.component.html',
    styleUrls: ['./textbox.component.scss']
  })
  export class TextboxComponent implements OnInit {
    // @Output() textboxContentEvent = new EventEmitter<string>()

    placeholder: string = 'Put your e-mail here, please'
    email: string = ''

    ngOnInit(): void{
      
    }

    pruebaEmail(): void{
      const validEmail = /^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
        // const kko = document.querySelector('#email')
        const textboxValue = (<HTMLInputElement>document.getElementById('email')).value
      
      if(!validEmail.exec(textboxValue)){
        console.log('email no valido');
      }
      else {
        console.log('email valido');
      }
    }
  }
  
